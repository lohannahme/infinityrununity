﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SoundManager : MonoBehaviour
{
    [SerializeField] private AudioClip[] _audioClips;
    private AudioSource _audioSource;

    public void Start()
    {
        GetAudioSourceComponent();
    }

    private void OnEnable()
    {
        EnableSounds();
    }

    private void OnDisable()
    {
        DisableSounds();
    }

    private void PlayCoinSound()
    {
        _audioSource.PlayOneShot(_audioClips[0]);
    }

    private void PlayDeadSound()
    {
        _audioSource.PlayOneShot(_audioClips[1]);
    }

    private void PlayJumpSound()
    {
        _audioSource.PlayOneShot(_audioClips[2]);
    }

    private void PlaySwipeSound()
    {
        _audioSource.PlayOneShot(_audioClips[3]);
    }

    private void PlayBoostSound()
    {
        _audioSource.PlayOneShot(_audioClips[4]);
    }

    private void DisableSounds()
    {
        Collectable.CoinsAtt -= PlayCoinSound;
        PlayerDamageManager.PlayerDeath -= PlayDeadSound;
        SwipeDetection.Jumped -= PlayJumpSound;
        SwipeDetection.LaneChange -= PlaySwipeSound;
        Boost.BoostActivate -= PlayBoostSound;
    }

    private void EnableSounds()
    {
        Collectable.CoinsAtt += PlayCoinSound;
        PlayerDamageManager.PlayerDeath += PlayDeadSound;
        SwipeDetection.Jumped += PlayJumpSound;
        SwipeDetection.LaneChange += PlaySwipeSound;
        Boost.BoostActivate += PlayBoostSound;
    }

    private void GetAudioSourceComponent()
    {
        _audioSource = gameObject.GetComponent<AudioSource>();
    }
}
