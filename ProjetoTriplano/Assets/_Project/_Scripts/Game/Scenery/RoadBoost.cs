﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadBoost : MonoBehaviour
{
    [SerializeField] private GameObject _boost = null;
    [SerializeField] private Vector2 _numberOfBoost;
    [SerializeField] private float[] _positionXBoost;

    public List<GameObject> _newCoins; // só funciona publica ?

    public void SetBoostPosition()
    {
        float minZPos = 10f;

        for (int i = 0; i < _newCoins.Count; i++)
        {
            float maxZPos = minZPos + 55f;
            float randomZPos = Random.Range(minZPos, maxZPos);
            _newCoins[i].transform.localPosition = new Vector3(_positionXBoost[Random.Range(0, 3)], -3, randomZPos);
            _newCoins[i].SetActive(true);
            minZPos = randomZPos + 1;
        }
    }

    void Start()
    {
        InstantiateBoosts();
        SetBoostPosition();
    }

    private void InstantiateBoosts()
    {
        int newNumberOfCollectables = (int)Random.Range(_numberOfBoost.x, _numberOfBoost.y);

        for (int i = 0; i < newNumberOfCollectables; i++)
        {
            _newCoins.Add(Instantiate(_boost, transform));
            _newCoins[i].SetActive(false);
        }
    }
}
