﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadObstacles : MonoBehaviour
{
    [SerializeField] private GameObject[] _obstacles;
    [SerializeField] private Vector2 _numberObstacles;
    [SerializeField] private List<GameObject> _newObstacles;
    [SerializeField] private float[] _positionXObstacles;

    private RoadCollectables _roadCollectables;

    public delegate void IncreaseSpeed();
    public static event IncreaseSpeed IncreasingSpeed;

    private void OnTriggerEnter(Collider other)
    {
        SetNewPositionObstacles(other);
    }

    void Start()
    {
        GetRoadCollectablesComponent();
        InstantiateObstacles();
        SetObstaclesPosition();
    }

    private void GetRoadCollectablesComponent()
    {
        _roadCollectables = gameObject.GetComponent<RoadCollectables>();
    }

    private void InstantiateObstacles()
    {
        int newNumberObstacles = (int)Random.Range(_numberObstacles.x, _numberObstacles.y);

        for (int i = 0; i < newNumberObstacles; i++)
        {
            _newObstacles.Add(Instantiate(_obstacles[Random.Range(0, _obstacles.Length)], transform));
            _newObstacles[i].SetActive(false);
        }
    }

    private void SetNewPositionObstacles(Collider other)
    {
        if (other.gameObject.layer == LayerNames.PLAYER)
        {
            transform.position = new Vector3(0, 3, transform.position.z + 148.7f * 2);
            SetObstaclesPosition();
            _roadCollectables.SetCoinsPosition();
            IncreasingSpeed?.Invoke();
        }
    }
    private void SetObstaclesPosition()
    {
        for (int i = 0; i < _newObstacles.Count; i++)
        {
            float minZPos = (148 / _newObstacles.Count) + (148 / _newObstacles.Count) * i;
            float maxZPos = (148 / _newObstacles.Count) + (148 / _newObstacles.Count) * i + 1;

            if(_newObstacles[i].TryGetComponent(out SphereCollider col))
            {
                _newObstacles[i].transform.localPosition = new Vector3(0, -4.17f, Random.Range(minZPos, maxZPos));
            }
            else
            {
                _newObstacles[i].transform.localPosition = new Vector3(_positionXObstacles[Random.Range(0, 2)], -4.17f, Random.Range(minZPos, maxZPos));

            }
            _newObstacles[i].SetActive(true);
        }
    }
}
