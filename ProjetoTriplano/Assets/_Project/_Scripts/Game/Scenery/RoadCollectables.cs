﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadCollectables : MonoBehaviour
{
    [SerializeField] private GameObject _collectable = null;
    [SerializeField] private Vector2 _numberOfCollectables;
    [SerializeField] private float[] _positionXObstacles;

    public List<GameObject> _newCoins; // só funciona publica ?

    public void SetCoinsPosition()
    {
        float minZPos = 10f;

        for (int i = 0; i < _newCoins.Count; i++)
        {
            float maxZPos = minZPos + 5f;
            float randomZPos = Random.Range(minZPos, maxZPos);
            _newCoins[i].transform.localPosition = new Vector3(_positionXObstacles[Random.Range(0, 3)], -3, randomZPos);
            _newCoins[i].SetActive(true);
            minZPos = randomZPos + 1;
        }
    }

    void Start()
    {
        InstantiateCoins();
        SetCoinsPosition();
    }

    private void InstantiateCoins()
    {
        int newNumberOfCollectables = (int)Random.Range(_numberOfCollectables.x, _numberOfCollectables.y);

        for (int i = 0; i < newNumberOfCollectables; i++)
        {
            _newCoins.Add(Instantiate(_collectable, transform));
            _newCoins[i].SetActive(false);
        }
    }
}
