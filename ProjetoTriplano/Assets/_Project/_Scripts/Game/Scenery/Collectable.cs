﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectable : MonoBehaviour
{
    public delegate void AttCoins();
    public static event AttCoins CoinsAtt;

    private void OnTriggerEnter(Collider other)
    {
        CollectObject(other);
    }

    private void CollectObject(Collider other)
    {
        if (other.gameObject.layer == LayerNames.PLAYER)
        {
            gameObject.SetActive(false);
            CoinsAtt?.Invoke();
        }
    }

}
