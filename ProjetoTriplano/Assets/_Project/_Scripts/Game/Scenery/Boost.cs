﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boost : MonoBehaviour
{
    public delegate void ActiveBoost();
    public static event ActiveBoost BoostActivate;

    private void OnTriggerEnter(Collider other)
    {
        CollectBoost(other);
    }

    private void CollectBoost(Collider other)
    {
        if (other.gameObject.layer == LayerNames.PLAYER)
        {
            gameObject.SetActive(false);
            BoostActivate?.Invoke();
        }
    }
}
