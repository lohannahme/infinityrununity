﻿using UnityEngine;

public class PlayerDamageManager : MonoBehaviour
{
    private PlayerManager _playerManager;
    private PlayerAnimatorController _playerAnimatorController;

    public delegate void DiePlayer();
    public static event DiePlayer PlayerDeath;

    private void Start()
    {
        GetComponents();
    }

    private void OnTriggerEnter(Collider other)
    {
        PlayerCollideObstacle(other);
    }

    private void PlayerCollideObstacle(Collider other)
    {
        if (other.gameObject.layer == LayerNames.OBSTACLE)
        {
            if ((int)_playerManager.score > _playerManager.maxScore)
            {
                SaveSystem.SaveMaxScore(_playerManager);
            }
            PlayerDeath?.Invoke();
            Debug.Log(_playerManager.maxScore);
            _playerAnimatorController.Die();
            _playerManager.Died();
        }
    }

    private void GetComponents()
    {
        _playerManager = gameObject.GetComponent<PlayerManager>();
        _playerAnimatorController = gameObject.GetComponent<PlayerAnimatorController>();
    }
}
