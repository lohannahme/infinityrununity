﻿using UnityEngine;

public class PlayerAnimatorController : MonoBehaviour
{
    private Animator _animator;
    public void Die()
    {
        _animator.SetBool(PlayerAnimatorParameters.DEATH, true);
    }
    public void Jump()
    {
        _animator.SetTrigger(PlayerAnimatorParameters.JUMP);
    }

    private void Start()
    {
        GetAnimatorComponent();
    }

    private void GetAnimatorComponent()
    {
        _animator = gameObject.GetComponent<Animator>();
    }
}
