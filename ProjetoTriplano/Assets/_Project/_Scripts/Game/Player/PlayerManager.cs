﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PlayerManager : MonoBehaviour
{
    [SerializeField] private float _speed;
    [SerializeField] private int _currentLane = 1;
    [SerializeField] private float _jumpHeight = 2;
    [SerializeField] private float _gravityValue = -8f;
    [SerializeField] private UIManager _uiManager;

    private Rigidbody _rigidbody;

    private bool _isGrounded = true;
    private bool _boost = false;

    private float _maxSpeed = 7;
    private float _increaseSpeed = 1.5f;
    private float _laneChange = 2f;
    private float _laneChangeSpeed = .8f;
    private float _jumpSpeed = .5f;
    private float _fallSpeed = .9f;
    private float _boostScore = 5f;
    private float _boostCoolDown = 5f;

    public int maxScore;
    public float score;

    public delegate void DiePlayer();
    public static event DiePlayer PlayerDied;

    public delegate void PlayerImpact();
    public static event PlayerImpact PlayerOnFloor;

    public void ChangeLane(int direction)
    {
        int targetLane = _currentLane + direction;
        if (targetLane < 0 || targetLane > 2)
            return;
        _currentLane = targetLane;

        if (direction > -1)
        {
            transform.DOMoveX((transform.position.x + _laneChange), _laneChangeSpeed);
        }
        else
        {
            transform.DOMoveX((transform.position.x - _laneChange), _laneChangeSpeed);
        }
    }
    public void Died()
    {
        _speed = 0;
        PlayerDied?.Invoke();
    }
    public void IncreaseSpeed()
    {
        _speed *= _increaseSpeed;
        if (_speed >= _maxSpeed)
        {
            _speed = _maxSpeed;
        }
    }

    public void Jump()
    {
        if (!_isGrounded)
        {
            return;
        }
        transform.DOMoveY(_jumpHeight, _jumpSpeed);
    }
    private void FixedUpdate()
    {
        Running();
        Gravity();
    }

    private void OnCollisionEnter(Collision collision)
    {
        CollisionOnFloor(collision);
    }

    private void OnCollisionExit(Collision collision)
    {
        CollisionExitFloor(collision);
    }

    private void OnEnable()
    {
        EnableIncreaseSpeed();
        Boost.BoostActivate += BoostOn;
    }

    private void OnDisable()
    {
        DisableIncreaseSpeed();
        Boost.BoostActivate -= BoostOn;
    }

    void Start()
    {
        GetRigidbodyComponent();
        LoadMaxscore();
    }

    private void Update()
    {
        UpdateScore();
    }

    private void CollisionExitFloor(Collision collision)
    {
        if (collision.gameObject.layer == LayerNames.FLOOR)
        {
            Debug.Log("ar");
            _isGrounded = false;
        }
    }

    private void CollisionOnFloor(Collision collision)
    {
        if (collision.gameObject.layer == LayerNames.FLOOR)
        {
            _isGrounded = true;
            Debug.Log("chao");
            PlayerOnFloor?.Invoke();
        }
    }
    private void DisableIncreaseSpeed()
    {
        RoadObstacles.IncreasingSpeed -= IncreaseSpeed;
    }

    private void EnableIncreaseSpeed()
    {
        RoadObstacles.IncreasingSpeed += IncreaseSpeed;
    }

    private void GetRigidbodyComponent()
    {
        _rigidbody = gameObject.GetComponent<Rigidbody>();
    }

    private void Gravity()
    {
        if (transform.position.y == _jumpHeight)
        {
            transform.DOMoveY(0, _fallSpeed);
        }
    }

    void LoadMaxscore() 
    { 
        PlayerData data = SaveSystem.LoadPlayerData();
        maxScore = data.maxScore;
    }

    private void Running()
    {
        _rigidbody.velocity = Vector3.forward * _speed;
    }

    private void UpdateScore()
    {
        if (_boost == false)
        {
            score += _speed * Time.deltaTime;
        }
        else 
        {
            score += _speed * _boostScore * Time.deltaTime;
        }
        _uiManager.ShowScore((int)score);
    }

    private void BoostOn()
    {
        _boost = true;
        StartCoroutine(BoostCoolDown());
    }

    private IEnumerator BoostCoolDown() 
    {
        yield return new WaitForSeconds(_boostCoolDown);
        _boost = false;
    }

}
