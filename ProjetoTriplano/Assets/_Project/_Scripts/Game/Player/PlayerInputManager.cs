﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


[DefaultExecutionOrder(-1)]
public class PlayerInputManager : MonoBehaviour
{
    public delegate void StartTouch(Vector2 position, float time);
    public event StartTouch OnStartTouch;
    public delegate void EndTouch(Vector2 position, float time);
    public event EndTouch OnEndTouch;

    private Camera _mainCamera;

    private PlayerInputActions _playerInputActions;

    public Vector2 PrimaryPosition()
    {
        return Utils.ScreenToWorld(_mainCamera, _playerInputActions.Player.PrimaryPosition.ReadValue<Vector2>());
    }

    private void Awake()
    {
        GenerateNewInputActions();
        GetCamera();
    }

    private void OnEnable()
    {
        EnablePlayerInputActions();
        UIManager.OpenGameOverCanvas += DisablePlayerInputActions;
    }

    private void OnDisable()
    {
        DisablePlayerInputActions();
        UIManager.OpenGameOverCanvas -= DisablePlayerInputActions;
    }
    void Start()
    {
        StartAndEndTouch();
    }
    private void DisablePlayerInputActions()
    {
        _playerInputActions.Disable();
    }
    private void EnablePlayerInputActions()
    {
        _playerInputActions.Enable();
    }
    private void EndTouchPrimary(InputAction.CallbackContext context)
    {
        if (OnEndTouch != null)
        {
            OnEndTouch(Utils.ScreenToWorld(_mainCamera, _playerInputActions.Player.PrimaryPosition.ReadValue<Vector2>()), (float)context.time);
        }
    }
    private void GenerateNewInputActions()
    {
        _playerInputActions = new PlayerInputActions();
    }
    private void GetCamera()
    {
        _mainCamera = Camera.main;
    }
    private void StartAndEndTouch()
    {
        _playerInputActions.Player.PrimaryContact.started += ctx => StartTouchPrimary(ctx);
        _playerInputActions.Player.PrimaryContact.canceled += ctx => EndTouchPrimary(ctx);
    }
    private void StartTouchPrimary(InputAction.CallbackContext context)
    {
        if (OnStartTouch != null)
        {
            OnStartTouch(Utils.ScreenToWorld(_mainCamera, _playerInputActions.Player.PrimaryPosition.ReadValue<Vector2>()), (float)context.startTime);
        }
    }

}
