﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerParticleManager : MonoBehaviour
{
    [SerializeField] private ParticleSystem _starParticle;
    [SerializeField] private ParticleSystem _explosionParticle;
    [SerializeField] private ParticleSystem _rightSideParticle;
    [SerializeField] private ParticleSystem _leftSideParticle;
    [SerializeField] private ParticleSystem _jumpParticle;
    [SerializeField] private ParticleSystem _boostParticle;

    private void OnEnable()
    {
        EnableParticles();
    }

    private void OnDisable()
    {
        DisableParticles();
    }

    void PlayParticle()
    {
        _starParticle.Play();
    }

    void ExplosionParticle()
    {
        _explosionParticle.Play();
    }

    void RighChangeLaneParticle()
    {
        _rightSideParticle.Play();
    }

    void LeftChangeLaneParticle()
    {
        _leftSideParticle.Play();
    }

    void JumpParticle()
    {
        _jumpParticle.Play();
    }

    void BoostParticle()
    {
        _boostParticle.Play();
    }

    private void DisableParticles()
    {
        Collectable.CoinsAtt -= PlayParticle;
        PlayerManager.PlayerDied -= ExplosionParticle;
        SwipeDetection.RighLane -= RighChangeLaneParticle;
        SwipeDetection.LeftLane -= LeftChangeLaneParticle;
        SwipeDetection.Jumped -= JumpParticle;
        Boost.BoostActivate -= BoostParticle;
    }

    private void EnableParticles()
    {
        Collectable.CoinsAtt += PlayParticle;
        PlayerManager.PlayerDied += ExplosionParticle;
        SwipeDetection.RighLane += RighChangeLaneParticle;
        SwipeDetection.LeftLane += LeftChangeLaneParticle;
        SwipeDetection.Jumped += JumpParticle;
        Boost.BoostActivate += BoostParticle;
    }
}
