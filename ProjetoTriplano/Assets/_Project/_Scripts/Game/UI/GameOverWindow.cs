﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOverWindow : MonoBehaviour
{
    [SerializeField] private Button _restartButton;

    private void AddButtonsListeners()
    {
        _restartButton.onClick.AddListener(RestartGame);
    }

    private void OnDisable()
    {
        DisableOpenGameOver();
    }

    private void OnEnable()
    {
        EnableOpenGameOver();
    }

    void Start()
    {
        SetScaleToZero();
        AddButtonsListeners();
    }

    private void DisableOpenGameOver()
    {
        UIManager.OpenGameOverCanvas -= Open;
    }

    private void EnableOpenGameOver()
    {
        UIManager.OpenGameOverCanvas += Open;
    }
    private void Open()
    {
        transform.LeanScale(Vector2.one, .5f);
    }

    private void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void SetScaleToZero()
    {
        transform.localScale = Vector2.zero;
    }
}
