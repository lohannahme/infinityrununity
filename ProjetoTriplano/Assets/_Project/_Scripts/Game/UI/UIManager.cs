﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] private PlayerManager _playerManager;
    [SerializeField] private TMP_Text _coinsText;
    [SerializeField] private TMP_Text _scoreText;
    [SerializeField] private TMP_Text _scoreGameOverText;
    [SerializeField] private TMP_Text _bestScoreText;
    [SerializeField] private Image _boostImage;

    private int _coins = 0;
    private float _score;
    private float _boostImageCooldown = 5f;

    public delegate void GameIsOver();
    public static event GameIsOver OpenGameOverCanvas;


    public void AddCoins()
    {
        _coins++;
        _coinsText.text = _coins.ToString();
    }
    public void GameOver()
    {
        _bestScoreText.text = $"{_playerManager.maxScore.ToString()} m";
        _scoreGameOverText.text = _scoreText.text;
        OpenGameOverCanvas?.Invoke();
    }

    public void ShowScore(int score)
    {
        _scoreText.text = $"{score.ToString()}m";
    }
    public void EnableBoostImage() 
    {
        if (_boostImage != null)
        {
            _boostImage.enabled = true;
            StartCoroutine(BoostEnableImageCooldown());
        }
    }

    public IEnumerator BoostEnableImageCooldown()
    {
        yield return new WaitForSeconds(_boostImageCooldown);
        _boostImage.enabled = false;
    }

    private void OnDisable()
    {
        DisableEvents();
    }

    private void OnEnable()
    {
        EnableEvents();
    }

    private void DisableEvents()
    {
        Collectable.CoinsAtt -= AddCoins;
        PlayerManager.PlayerDied -= GameOver;
        Boost.BoostActivate -= EnableBoostImage;
    }

    private void EnableEvents()
    {
        Collectable.CoinsAtt += AddCoins;
        PlayerManager.PlayerDied += GameOver;
        Boost.BoostActivate += EnableBoostImage;
    }
}
