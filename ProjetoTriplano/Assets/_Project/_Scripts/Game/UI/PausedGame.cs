﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PausedGame : MonoBehaviour
{
    [SerializeField] private Button _pauseButton;
    [SerializeField] private Button _resumeButton;
    [SerializeField] private GameObject _pauseImage;

    void Start()
    {
        AddButtonsListeners();
        _pauseImage.gameObject.SetActive(false);
    }

    private void AddButtonsListeners()
    {
        _pauseButton.onClick.AddListener(PauseGame);
        _resumeButton.onClick.AddListener(ResumeGame);
    }

    private void PauseGame()
    {
        Debug.Log("pauseBut");
        _pauseImage.gameObject.SetActive(true);
        Time.timeScale = 0f;
    }

    private void ResumeGame()
    {
        _pauseImage.gameObject.SetActive(false);
        Time.timeScale = 1f;
    }

}
