﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeDetection : MonoBehaviour
{
    [SerializeField] private float _minDistance = .2f;
    [SerializeField] private float _maxTime = 1f;
    [SerializeField, Range(0f, 1f)] private float _directionThreshould = 0.655f;
    [SerializeField] private PlayerManager _playerManager = null;
    [SerializeField] private PlayerAnimatorController _playerAnimatorController = null;

    private PlayerInputManager _playerInputManager;
    
    private Vector2 _startPosition;
    private float _startTime;

    private Vector2 _endPosition;
    private float _endTime;

    public delegate void JumpControl();
    public static event JumpControl Jumped;

    public delegate void ChangeLane();
    public static event ChangeLane LaneChange;

    public static event ChangeLane RighLane;
    public static event ChangeLane LeftLane;

    private void Awake()
    {
        GetInputManagerComponent();
    }
    private void OnDisable()
    {
        DisableStartAndEndTouch();
    }

    private void OnEnable()
    {
        EnableStartAndEndTouch();
    }

    private void DetectSwipe()
    {
        if(Vector3.Distance(_startPosition, _endPosition) >= _minDistance && (_endTime - _startTime) <= _maxTime)
        {
            Debug.DrawLine(_startPosition, _endPosition, Color.yellow, 5f);
            Vector3 direction = _endPosition - _startPosition;
            Vector2 direction2D = new Vector2(direction.x, direction.y).normalized;
            SwipeDirection(direction2D);
        }
    }

    private void DisableStartAndEndTouch()
    {
        _playerInputManager.OnStartTouch -= SwipeStart;
        _playerInputManager.OnEndTouch -= SwipeEnd;
    }

    private void EnableStartAndEndTouch()
    {
        _playerInputManager.OnStartTouch += SwipeStart;
        _playerInputManager.OnEndTouch += SwipeEnd;
    }

    private void GetInputManagerComponent()
    {
        _playerInputManager = gameObject.GetComponent<PlayerInputManager>();
    }

    private void SwipeDirection(Vector2 direction)
    {
        if (Vector2.Dot(Vector2.up, direction) > _directionThreshould)
        {
            _playerManager.Jump();
            _playerAnimatorController.Jump();
            Jumped?.Invoke();
        }

        else if (Vector2.Dot(Vector2.left, direction) > _directionThreshould)
        {
            _playerManager.ChangeLane(-1);
            LaneChange?.Invoke();
            LeftLane?.Invoke();
        }

        else if (Vector2.Dot(Vector2.right, direction) > _directionThreshould)
        {
            _playerManager.ChangeLane(1);
            LaneChange?.Invoke();
            RighLane?.Invoke();
        }
    }
    private void SwipeEnd(Vector2 position, float time)
    {
        _endPosition = position;
        _endTime = time;
        DetectSwipe();
    }

    private void SwipeStart(Vector2 position, float time)
    {
        _startPosition = position;
        _startTime = time;
    }
}
