﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneLoading : MonoBehaviour
{
    [SerializeField] private Image _progressBar;

    void Start()
    {
        StartCoroutine(LoadAsynOperation());
    }

    IEnumerator LoadAsynOperation()
    {
        AsyncOperation gamelevel = SceneManager.LoadSceneAsync(2);

        while(gamelevel.progress > 1)
        {
            _progressBar.fillAmount = gamelevel.progress;
            yield return new WaitForEndOfFrame();
        }
    }
}
